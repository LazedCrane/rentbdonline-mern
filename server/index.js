//import { GraphQLServer } from 'graphql-yoga'
// ... or using `require()`
require('dotenv').config();
const user = process.env.DATABASE_USER;
const pass = encodeURIComponent(process.env.DATABASE_PASS);
const dbDomain = process.env.DATABASE_DOMAIN;
const port = process.env.DATABASE_PORT;
const dbName = process.env.DATABASE_NAME;
const dbURi = `mongodb://${user}:${pass}@${dbDomain}:${port}/${dbName}`;

const { GraphQLServer } = require('graphql-yoga')

const mongoose = require("mongoose");
mongoose.connect(dbURi, {useNewUrlParser: true});

const resolvers = require("./graphql/resolvers");
const typeDefs = `${__dirname}/graphql/schema.graphql`;

const server = new GraphQLServer({ typeDefs, resolvers });
mongoose.connection.once("open", function() {
    server.start(() => console.log('Server is running on localhost:4000')) 
});