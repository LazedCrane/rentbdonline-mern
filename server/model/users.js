const mongoose = require("mongoose");

const rbdUsers = mongoose.model('rbdUsers', {
    first_name: String,
    middle_name: String,
    last_name: String,
    gender: Number,
    username: String,
    role: String
});

module.exports = rbdUsers;