const rbdUsers = require("../model/users");
const resolvers = {
    Query: {
        hello: (_, { name }) => `Hello ${name || 'World'}`,
        allRbdUsers: () => rbdUsers.find()
    },

    Mutation: {
        addUser: async(_, { first_name, middle_name, last_name, gender, username, role }) => {
            const rbdUser = new rbdUsers({
                first_name, middle_name, last_name, gender, username, role
            });

            await rbdUser.save();
            return rbdUser;
        },
        updateUser: async (_, updateDataObj) => {
            const id = updateDataObj.id;
            delete updateDataObj.id;
            const isUpdated = await rbdUsers.findByIdAndUpdate(id, updateDataObj);
            return isUpdated;
        },

        removeUser: async (_, {id}) => {
            await rbdUsers.findByIdAndRemove(id);
            return true;
        }
    }
}

module.exports = resolvers;